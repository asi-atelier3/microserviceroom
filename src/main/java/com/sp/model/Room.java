package com.sp.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;

@Entity
public class Room {
    @Id
    @GeneratedValue
    private long id;
    private long creatorId;
    private long adversaryId;
    private long bet;
    private long creatorCardId;
    private long adversaryCardId;

    public Room() {
    }

    public Room(long creatorId, long bet) {
        this.creatorId = creatorId;
        this.bet = bet;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatorId() {
        return this.creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public long getAdversaryId() {
        return this.adversaryId;
    }

    public void setAdversaryId(long adversaryId) {
        this.adversaryId = adversaryId;
    }

    public long getBet() {
        return this.bet;
    }

    public void setBet(long bet) {
        this.bet = bet;
    }

    public long getCreatorCardId() {
        return this.creatorCardId;
    }

    public void setCreatorCardId(long creatorCardId) {
        this.creatorCardId = creatorCardId;
    }

    public long getAdversaryCardId() {
        return this.adversaryCardId;
    }

    public void setAdversaryCardId(long adversaryCardId) {
        this.adversaryCardId = adversaryCardId;
    }

    @Override
    public String toString() {
        return "{" +
                " id='" + getId() + "'" +
                ", creatorId='" + getCreatorId() + "'" +
                ", adversaryId='" + getAdversaryId() + "'" +
                ", bet='" + getBet() + "'" +
                ", creatorCardId='" + getCreatorCardId() + "'" +
                ", adversaryCardId='" + getAdversaryCardId() + "'" +
                "}";
    }

}

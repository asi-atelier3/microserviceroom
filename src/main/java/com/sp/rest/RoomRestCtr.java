package com.sp.rest;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.Room;

import com.sp.service.RoomService;
@CrossOrigin
@RestController
@RequestMapping("/api/rooms")
public class RoomRestCtr {
    @Autowired
    RoomService roomService;
    
    /*
    * GET /rooms
    */
    @RequestMapping(method=RequestMethod.GET,value="")
    public ArrayList<Room> getRooms() {
        ArrayList<Room> rooms=roomService.getRooms();
        return rooms;
    }
    
    /*
    * POST /rooms?uid=uid&bet=bet
    */
    @RequestMapping(method=RequestMethod.POST,value="")
    public Room addRoom(@RequestParam(value="uid",required=true) long uid, @RequestParam(value="bet",required=true) long bet) {
        Room room = new Room(uid,bet);
        return roomService.addRoom(room);
    }
    
    /*
     * PUT /rooms/:id?uid=uid&cid=cid
     */
    @RequestMapping(method=RequestMethod.PUT,value="/{id}")
    public boolean updateRoom(@PathVariable long id ,@RequestParam(required = true) Long uid, @RequestParam(required = true) Long cid) {
        Room room = roomService.getRoomById(id);
        if(uid == room.getCreatorId()) room.setCreatorCardId(cid);
        else {
            room.setAdversaryId(uid);
            room.setAdversaryCardId(cid);
        }
        return roomService.updateRoom(Long.valueOf(id), room);
    }

    /*
     * GET /rooms/:id
     */
    @RequestMapping(method=RequestMethod.GET,value="/{id}")
    public Room getRoom(@PathVariable String id) {
        Room room = roomService.getRoomById(Long.valueOf(id));
        return room;
    }
    
    /*
     * DELETE /rooms/:id
     */
    @RequestMapping(method=RequestMethod.DELETE,value="/{id}")
    public boolean deleteRoom(@PathVariable String id) {
        return roomService.deleteRoom(Long.valueOf(id));
    }
}

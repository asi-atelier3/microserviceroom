# Run
FROM openjdk:11-jre
ENV ROOMSERVICE_HOME /opt/roomservice
WORKDIR $ROOMSERVICE_HOME
COPY ./target/*.jar $ROOMSERVICE_HOME/roomservice.jar

EXPOSE 8083

ENTRYPOINT java -jar roomservice.jar